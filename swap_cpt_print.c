/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_cpt_print.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 10:36:47 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 18:54:00 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	sa(s_a);
	c_op->sa++;
	add_to_answer(c_op->answer, "sa");
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nsa\n" : "sa ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	swap_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	sb(s_b);
	c_op->sb++;
	add_to_answer(c_op->answer, "sb");
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nsb\n" : "sb ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	swap_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	ss(s_a, s_b);
	c_op->ss++;
	add_to_answer(c_op->answer, "ss");
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nss\n" : "ss ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}
