/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_analitic.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/03 14:36:37 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 16:32:22 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	get_max(t_stack stack)
{
	int	max;
	int	i;

	i = 0;
	max = stack.tab[i];
	while (++i < stack.size)
		if (stack.tab[i] > max)
			max = stack.tab[i];
	return (max);
}

int	is_sort_until(t_stack stack, int rv)
{
	int i;
	int	*mem_tab;
	int	tempo;

	if ((mem_tab = (int *)malloc(sizeof(int) * stack.size)) == NULL)
		return (-2);
	i = -1;
	while (++i < stack.size)
		mem_tab[i] = stack.tab[i];
	while (is_sort(mem_tab, stack.size) == 0)
	{
		i = -1;
		while (++i < stack.size - 1)
			if (mem_tab[i] > mem_tab[i + 1])
			{
				tempo = mem_tab[i];
				mem_tab[i] = mem_tab[i + 1];
				mem_tab[i + 1] = tempo;
			}
	}
	i = rv > 0 ? 0 : 1 - stack.size;
	while (mem_tab[-i + ((stack.size - 1) * rv)] == stack.tab[rv > 0 ? i : -i])
		if (++i == 0 && rv == 0)
			return (-i);
	return (rv > 0 ? i - 1 : -i);
}

int	is_there_below(t_stack stack, int val)
{
	int i;

	i = -1;
	while (++i < stack.size)
		if (val > stack.tab[i])
			return (1);
	return (0);
}

int	is_there_above(t_stack stack, int val)
{
	int i;

	i = -1;
	while (++i < stack.size)
		if (val <= stack.tab[i])
			return (1);
	return (0);
}

int	is_sort(const int *stack, int size)
{
	int	i;

	i = -1;
	while (++i < size - 1)
		if (stack[i] > stack[i + 1])
			return (0);
	return (1);
}
