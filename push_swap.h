/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 10:12:17 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 16:52:51 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "./libft/libft.h"
# include "./libftprintf/ft_printf.h"
# include <stdlib.h>

# define P_ERROR 'p'
# define CPT_OP 'c'
# define OP_OP 'o'
# define SHOW_STACK 's'
# define SHOW_END_STACK 'e'
# define SHOW_BEGIN_STACK 'b'
# define ALL_FLAGS 'a'
# define HELP_FLAG 'h'
# define CHK_SUCCES 'S'

# define STR_SIZE 500

typedef struct	s_sflags
{
	char	pars_error_flag;
	char	cpt_op_flag;
	char	op_by_op_flag;
	char	show_stack_flag;
	char	show_begin_stack;
	char	show_end_stack_flag;
	char	check_succes_flag;
}				t_sflags;

typedef struct	s_solution
{
	char				str[STR_SIZE];
	struct s_solution	*next;
}				t_solution;

typedef struct	s_op_cpt
{
	int			pa;
	int			pb;
	int			sa;
	int			sb;
	int			ss;
	int			ra;
	int			rb;
	int			rr;
	int			rra;
	int			rrb;
	int			rrr;
	t_solution	*answer;
	t_sflags	*flag;
}				t_op_cpt;

typedef struct	s_stack
{
	int		size;
	int		*tab;
}				t_stack;

void			sa(t_stack *stack_a);
void			sb(t_stack *stack_b);
void			ss(t_stack *stack_a, t_stack *stack_b);

void			ra(t_stack *stack_a);
void			rb(t_stack *stack_b);
void			rr(t_stack *stack_a, t_stack *stack_b);

void			rra(t_stack *stack_a);
void			rrb(t_stack *stack_b);
void			rrr(t_stack *stack_a, t_stack *stack_b);

void			pa(t_stack *stack_a, t_stack *stack_b);
void			pb(t_stack *stack_a, t_stack *stack_b);

void			swap_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			swap_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			swap_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);

void			rotate_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			rotate_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			rotate_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);

void			r_rotate_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			r_rotate_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);
void			r_rotate_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *cp);

void			push_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);
void			push_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);

int				get_args(char **val, t_stack *s_a, t_sflags flags);
int				get_flags(char **argv, t_sflags *flags);
int				set_up_stacks(t_stack *s_a, t_stack *s_b, int sz);

void			show_stack(t_stack stack);
void			show_stack_ab(t_stack stack_a, t_stack stack_b);
void			show_op(t_op_cpt c_op);

void			counter_init(t_op_cpt *cpt);
void			solve(t_stack *s_a, t_stack *s_b, t_sflags flags);

int				get_split_point(t_stack stack);
int				get_split_point_a(t_stack stack);
int				get_split_point_b(t_stack stack);
int				is_sort(const int *stack, int size);
int				is_there_above(t_stack stack, int	val);
int				is_there_below(t_stack stack, int	val);
int				is_sort_until(t_stack stack, int rev);

void			opt_swap_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);
void			opt_swap_b(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);
void			rot_clst_a(t_stack *s_a, t_stack *s_b, int val, t_op_cpt *c_op);
void			rot_clst_b(t_stack *s_a, t_stack *s_b, int val, t_op_cpt *c_op);
void			rot_to_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op, int val);

void			rotate_until_sort_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);
void			rotate_until_sort_b(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);

void			sort_a(t_stack *s_a, t_stack *s_b, t_sflags flg, t_op_cpt *c_p);
void			big_solve(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op);

int				add_answer_string(t_op_cpt *cpt);
void			add_to_answer(t_solution *answer, char *str_cmd);
void			print_solution(t_solution *answer);
void			wait_for_enter(void);

#endif
