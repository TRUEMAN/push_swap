/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_until_sort.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:50:05 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 17:16:14 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		*sorted_stack(t_stack *s_a)
{
	int	*solved_stack;
	int	tempo;
	int	i;

	if ((solved_stack = ft_memalloc(sizeof(int) * s_a->size)) == NULL)
		return (solved_stack);
	i = -1;
	while (++i < s_a->size)
		solved_stack[i] = s_a->tab[i];
	while (is_sort(solved_stack, s_a->size) == 0)
	{
		i = -1;
		while (++i < s_a->size - 1)
			if (solved_stack[i] > solved_stack[i + 1])
			{
				tempo = solved_stack[i];
				solved_stack[i] = solved_stack[i + 1];
				solved_stack[i + 1] = tempo;
			}
	}
	return (solved_stack);
}

static void		rot_na(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rotate_a_cpt_print(s_a, s_b, c_op);
	swap_a_cpt_print(s_a, s_b, c_op);
}

static void		opt_rot(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op, int *solved)
{
	int	i;
	int	max;
	int	max_index;

	i = 0;
	max = s_a->tab[0];
	max_index = 0;
	while (++i < s_a->size)
		if (s_a->tab[i] > max)
		{
			max_index = i;
			max = s_a->tab[i];
		}
	if (max_index == s_a->size - 1)
		return ;
	if (max_index >= s_a->size / 2)
		while (s_a->tab[s_a->size - 1] != solved[s_a->size - 1])
			r_rotate_a_cpt_print(s_a, s_b, c_op);
	else
		while (s_a->tab[s_a->size - 1] != solved[s_a->size - 1])
			rotate_a_cpt_print(s_a, s_b, c_op);
}

static void		rot_mst(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op, int *solved)
{
	int			i;

	if (s_a->size <= 2)
		return ;
	i = 0;
	if (s_a->size == 3 && s_a->tab[0] > s_a->tab[1] &&
		s_a->tab[2] < s_a->tab[1])
		return (rot_na(s_a, s_b, c_op));
	while (i < s_a->size && (s_a->tab[i] == solved[i]))
		++i;
	if (i == s_a->size - 2)
	{
		r_rotate_a_cpt_print(s_a, s_b, c_op);
		if (s_a->size > 3)
			r_rotate_a_cpt_print(s_a, s_b, c_op);
		if (s_a->tab[0] > s_a->tab[1])
			swap_a_cpt_print(s_a, s_b, c_op);
		rotate_until_sort_a(s_a, s_b, c_op);
		return ;
	}
	opt_swap_a(s_a, s_b, c_op);
	opt_rot(s_a, s_b, c_op, solved);
}

void			rotate_until_sort_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	int	*slved_stk;
	int	i;
	int	j;

	if (is_sort(s_a->tab, s_a->size) || (slved_stk = sorted_stack(s_a)) == NULL)
		return ;
	i = -1;
	j = 0;
	while (slved_stk[0] != s_a->tab[j])
		++j;
	while (++i < s_a->size)
	{
		if (slved_stk[i] != s_a->tab[j < s_a->size ? j : j - s_a->size])
		{
			rot_mst(s_a, s_b, c_op, slved_stk);
			return ;
		}
		++j;
	}
	while (is_sort(s_a->tab, s_a->size) == 0)
		rot_to_a(s_a, s_b, c_op, slved_stk[0]);
	ft_memdel((void **)&slved_stk);
}
