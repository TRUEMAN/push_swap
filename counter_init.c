/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   counter_init.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/03 10:51:04 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 16:45:07 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <unistd.h>

static int		add_answer_block(t_solution *answer)
{
	int	i;

	if (answer->next == NULL)
	{
		answer->next = (t_solution*)malloc(sizeof(t_solution));
		i = -1;
		while (++i < STR_SIZE && answer->next != NULL)
			answer->next->str[i] = '\0';
		if (answer->next != NULL)
			answer->next->next = NULL;
		return (answer->next != NULL ? 1 : 0);
	}
	return (add_answer_block(answer->next));
}

int				add_answer_string(t_op_cpt *cpt)
{
	int	i;

	i = 0;
	if (cpt->answer == NULL)
	{
		cpt->answer = (t_solution*)malloc(sizeof(t_solution));
		while (++i < STR_SIZE && cpt->answer != NULL)
			cpt->answer->str[i] = '\0';
		if (cpt->answer != NULL)
			cpt->answer->next = NULL;
		return (cpt->answer != NULL ? 1 : 0);
	}
	return (add_answer_block(cpt->answer));
}

void			counter_init(t_op_cpt *cpt)
{
	cpt->pa = 0;
	cpt->pb = 0;
	cpt->sa = 0;
	cpt->sb = 0;
	cpt->ss = 0;
	cpt->ra = 0;
	cpt->rb = 0;
	cpt->rr = 0;
	cpt->rra = 0;
	cpt->rrb = 0;
	cpt->rrr = 0;
	cpt->answer = NULL;
	add_answer_string(cpt);
}

void			print_solution(t_solution *answer)
{
	write(1, answer->str, ft_strlen(answer->str));
	if (answer->next != NULL)
	{
		ft_putchar(' ');
		print_solution(answer->next);
	}
	else
		ft_putchar('\n');
}

void			add_to_answer(t_solution *answer, char *str_cmd)
{
	int i;

	while (answer->next != NULL)
		answer = answer->next;
	i = 0;
	while (i + 4 < STR_SIZE && answer->str[i] != '\0')
		i++;
	if (i + 4 >= STR_SIZE)
	{
		add_answer_block(answer);
		answer = answer->next;
	}
	if (answer != NULL)
	{
		if (i != 0)
		{
			answer->str[i] = ' ';
			i++;
		}
		strcpy(answer->str + i, str_cmd);
	}
}
