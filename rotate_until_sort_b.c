/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_until_sort_b.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 15:32:04 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 15:42:07 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		*sorted_stack(t_stack *s_a)
{
	int	*solved_stack;
	int	tempo;
	int	i;

	if ((solved_stack = ft_memalloc(sizeof(int) * s_a->size)) == NULL)
		return (solved_stack);
	i = -1;
	while (++i < s_a->size)
		solved_stack[i] = s_a->tab[i];
	while (is_sort(solved_stack, s_a->size) == 0)
	{
		i = -1;
		while (++i < s_a->size - 1)
			if (solved_stack[i] > solved_stack[i + 1])
			{
				tempo = solved_stack[i];
				solved_stack[i] = solved_stack[i + 1];
				solved_stack[i + 1] = tempo;
			}
	}
	return (solved_stack);
}

void			rotate_until_sort_b(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	int	*slved_stk;

	if (s_b->size <= 2)
		return ;
	if (is_sort(s_b->tab, s_b->size) || (slved_stk = sorted_stack(s_b)) == NULL)
		return ;
	if (s_b->tab[1] == slved_stk[s_b->size - 1])
		swap_b_cpt_print(s_a, s_b, c_op);
	while (slved_stk[0] != s_b->tab[s_b->size - 1])
		r_rotate_b_cpt_print(s_a, s_b, c_op);
	ft_memdel((void **)&slved_stk);
}
