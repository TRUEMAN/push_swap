/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_up_stacks.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 15:58:40 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 15:42:32 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	set_up_stacks(t_stack *s_a, t_stack *s_b, int size)
{
	s_a->size = size;
	if ((s_a->tab = (int*)malloc(sizeof(int) * size)) == NULL)
		return (-1);
	s_b->size = 0;
	if ((s_b->tab = (int*)malloc(sizeof(int) * size)) == NULL)
	{
		free(s_a->tab);
		return (-1);
	}
	return (1);
}
