/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reg_str.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 14:47:58 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:34:53 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <unistd.h>

int					ft_fb_putstr(const char *str)
{
	int	len;

	len = 0;
	while (str[len] != '\0')
		++len;
	write(1, str, len);
	return (len);
}

static const char	*ft_set_start(const char *str)
{
	if (ft_strrchr(str, '%') != str)
		str = ft_strrchr(str, '%');
	else
	{
		++str;
		while (*str == '-' || *str == '.' || *str == ' ' || ft_isdigit(*str) ||
				*str == '.')
			++str;
	}
	return (str);
}

int					ft_print_percent(t_data dt, const char *str)
{
	intmax_t	nb_printed;

	str = ft_set_start(str);
	nb_printed = 0;
	if ((dt.format_flag & minus_flag) == 0)
	{
		while (dt.min_size > ++nb_printed)
			ft_putchar((dt.format_flag & zero_flag) ? '0' : ' ');
		--nb_printed;
		nb_printed += ft_fb_putstr(str);
	}
	else
	{
		nb_printed += ft_fb_putstr(str);
		--nb_printed;
		while (dt.min_size > ++nb_printed)
			ft_putchar((dt.format_flag & zero_flag) ? '0' : ' ');
	}
	return (nb_printed);
}

int					ft_print_reg_str(const char *str)
{
	uintmax_t	len;

	len = 0;
	while (str[len] != '\0')
		++len;
	write(1, str, len);
	return (len);
}
