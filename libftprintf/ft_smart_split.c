/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_smart_split.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 11:14:33 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:36:11 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	**ft_create_return_tab(const char *str, int *nb_elem)
{
	int		i;
	int		j;
	char	**splt_str;

	i = -1;
	j = 1;
	while (str[++i] != '\0')
		if (str[i] == '%')
			j += 2;
	if ((splt_str = (char **)malloc(sizeof(char *) * (j + 1))) == NULL)
		return (NULL);
	i = -1;
	while (++i < (j + 1))
	{
		splt_str[i] = NULL;
	}
	*nb_elem = j + 1;
	return (splt_str);
}

int		ft_is_end_id(const char *str, int id, int *i)
{
	if ((id % 2 == 0) && ((*str == 's') || (*str == 'S') || (*str == 'p') ||
		(*str == 'd') || (*str == 'D') || (*str == 'i') || (*str == 'o') ||
		(*str == 'O') || (*str == 'u') || (*str == 'U') || (*str == 'x') ||
		(*str == 'X') || (*str == 'c') || (*str == 'C') || (*str == '%') ||
		(*str == 'Z')))
	{
		++*i;
		return (1);
	}
	if ((*str == '\0') || ((id % 2 == 1) && ((*(str) == '%'))))
	{
		return (1);
	}
	return (0);
}

void	ft_free_all(char **str, int *nb_elem)
{
	int		i;

	i = -1;
	while (++i < *nb_elem)
		free(str[i]);
	free(str);
	str = NULL;
}

void	ft_fill_tab(const char *str, char **splt_str, int *nb_elem)
{
	int		i;
	int		j;
	int		k;
	int		id;

	i = 1;
	j = 0;
	k = 0;
	id = (str[i - 1] == '%') ? 0 : 1;
	while (str[i - 1] != '\0')
	{
		while (ft_is_end_id(str + i, id, &i) == 0)
			++i;
		if ((splt_str[k] = ft_strsub(str, j, i - j)) == NULL)
		{
			ft_free_all(splt_str, nb_elem);
			return ;
		}
		if (str[i] == '\0' || str[i] < 0)
			break ;
		j = i;
		++i;
		++k;
		id = (str[j] == '%') ? 0 : 1;
	}
}

char	**ft_smart_split(const char *str, int *nb_elem)
{
	char	**splt_str;

	if ((splt_str = ft_create_return_tab(str, nb_elem)) == NULL)
		return (NULL);
	if ((ft_strlen(str) <= 1) || (ft_strrchr(str, '%') == NULL))
		splt_str[0] = ft_strdup(str);
	else
		ft_fill_tab(str, splt_str, nb_elem);
	return (splt_str);
}
