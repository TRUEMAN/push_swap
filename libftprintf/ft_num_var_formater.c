/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_num_var_formater.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 11:41:25 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:33:32 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static uintmax_t	ft_mask_size_poux(t_data dt)
{
	uintmax_t	unsigned_mask;
	int			size;

	unsigned_mask = 0;
	if (ft_strstr(dt.size_flag, "ll"))
		size = (sizeof(unsigned long long) * 8);
	else if (ft_strstr(dt.size_flag, "l"))
		size = (sizeof(unsigned long) * 8);
	else if (ft_strstr(dt.size_flag, "hh"))
		size = (sizeof(unsigned char) * 8);
	else if (ft_strstr(dt.size_flag, "h"))
		size = (sizeof(unsigned short) * 8);
	else if (ft_strstr(dt.size_flag, "z"))
		size = (sizeof(size_t) * 8);
	else if (ft_strstr(dt.size_flag, "j"))
		size = (sizeof(uintmax_t) * 8);
	else
		size = (sizeof(int) * 8);
	while (--size >= 0)
		unsigned_mask = unsigned_mask * 2 + 1;
	return (unsigned_mask);
}

static uintmax_t	ft_mask_size_di(t_data dt)
{
	uintmax_t	unsigned_mask;
	int			size;

	unsigned_mask = 0;
	if (ft_strstr(dt.size_flag, "ll"))
		size = (sizeof(long long) * 8);
	else if (ft_strstr(dt.size_flag, "l"))
		size = (sizeof(long) * 8);
	else if (ft_strstr(dt.size_flag, "hh"))
		size = (sizeof(signed char) * 8);
	else if (ft_strstr(dt.size_flag, "h"))
		size = (sizeof(short) * 8);
	else if (ft_strstr(dt.size_flag, "z"))
		size = (sizeof(size_t) * 8);
	else if (ft_strstr(dt.size_flag, "j"))
		size = (sizeof(intmax_t) * 8);
	else
		size = (sizeof(int) * 8);
	while (--size >= 0)
		unsigned_mask = unsigned_mask * 2 + 1;
	return (unsigned_mask);
}

uintmax_t			ft_num_var_formater(uintmax_t val, t_data dt)
{
	uintmax_t	unsigned_mask;

	if (dt.var_id == 'd' || dt.var_id == 'i' || dt.var_id == 'D')
		unsigned_mask = ft_mask_size_di(dt);
	else
		unsigned_mask = ft_mask_size_poux(dt);
	val = (val & unsigned_mask);
	return (val);
}

int					ft_num_length(uintmax_t val, char is_sgnd, t_data dt)
{
	int			len;
	int			base;
	intmax_t	unsigned_mask;

	if (dt.var_id == 'd' || dt.var_id == 'i' || dt.var_id == 'D')
		unsigned_mask = ft_mask_size_di(dt);
	else
		unsigned_mask = ft_mask_size_poux(dt);
	val = (val & unsigned_mask);
	base = 10;
	if (dt.var_id == 'o' || dt.var_id == 'O' || dt.var_id == 'x' ||
		dt.var_id == 'X' || dt.var_id == 'p')
		base = (dt.var_id == 'o' || dt.var_id == 'O') ? 8 : 16;
	len = 1;
	len += (base != 10) && (dt.format_flag & sharp_flag) ? base / 8 : 0;
	if (is_sgnd == 3 || (dt.format_flag & plus_flag))
		++len;
	while ((val = val / base) > 0)
		++len;
	return (len);
}
