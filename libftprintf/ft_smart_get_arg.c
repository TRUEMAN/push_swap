/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_smart_get_arg.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 16:37:49 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:36:02 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_get_var_id(char const *splt_str, t_data *dt)
{
	char	var_id;

	while (*splt_str != '\0')
		++splt_str;
	var_id = *(splt_str - 1);
	dt->var_id = var_id;
	if (var_id == 's' || var_id == 'o' || var_id == 'u' || var_id == 'x' ||
		var_id == 'd' || var_id == 'i' || var_id == 'c' || var_id == 'p' ||
		var_id == 'X' || var_id == '%')
	{
		dt->var_id = var_id;
		return (1);
	}
	else if (var_id == 'O' || var_id == 'U' || var_id == 'D'
			|| var_id == 'S' || var_id == 'C')
	{
		dt->size_flag = "l";
		dt->var_id = var_id;
		return (1);
	}
	dt->var_id = '%';
	return (1);
}

int	ft_get_size_flag(char const *splt_str, char const *flag_adr, t_data *dt)
{
	int	retest;

	retest = (splt_str == flag_adr) ? 1 : 0;
	if ((retest) && (!(dt->var_id == 'O' || dt->var_id == 'U' ||
		dt->var_id == 'D' || dt->var_id == 'S' || dt->var_id == 'C')))
		dt->size_flag = "";
	if ((flag_adr = ft_strstr(splt_str, "ll")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "ll" : "err";
	else if ((flag_adr = ft_strstr(splt_str, "l")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "l" : "err";
	else if ((flag_adr = ft_strstr(splt_str, "hh")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "hh" : "err";
	else if ((flag_adr = ft_strstr(splt_str, "h")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "h" : "err";
	else if ((flag_adr = ft_strstr(splt_str, "j")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "j" : "err";
	else if ((flag_adr = ft_strstr(splt_str, "z")) != NULL)
		dt->size_flag = (dt->size_flag[0] == '\0') ? "z" : "err";
	if (retest == 1 && (flag_adr != NULL))
		ft_get_size_flag(flag_adr + ft_strlen((dt->size_flag)), NULL, dt);
	return ((ft_strcmp("err", (dt->size_flag)) == 0) ? 0 : 1);
}

int	ft_get_frmt_flag(char const *splt_str, char const *flag_adr, t_data *dt)
{
	if ((flag_adr = ft_strstr(splt_str, "#")) != NULL)
		dt->format_flag += (dt->format_flag & 0x01) == 0 ? 1 : 0;
	if (((flag_adr = ft_strstr(splt_str, "0")) != NULL) &&
		!ft_isdigit(*(flag_adr - 1)) && ((ft_strrchr(flag_adr, '.') == NULL) ||
		(dt->var_id == 's' || dt->var_id == 'S' || dt->var_id == 'c' ||
		dt->var_id == 'C' || dt->var_id == '%')))
		dt->format_flag += (dt->format_flag & 0x02) == 0 ? 2 : 0;
	if ((flag_adr = ft_strstr(splt_str, "-")) != NULL)
		dt->format_flag += (dt->format_flag & 0x04) == 0 ? 4 : 0;
	if ((flag_adr = ft_strstr(splt_str, "+")) != NULL)
		dt->format_flag += (dt->format_flag & 0x08) == 0 ? 8 : 0;
	if ((flag_adr = ft_strstr(splt_str, " ")) != NULL)
		dt->format_flag += (dt->format_flag & 0x10) == 0 ? 16 : 0;
	return (1);
}

int	ft_get_precision(char const *splt_str, char const *flag_adr, t_data *dt)
{
	splt_str = ft_strrchr(splt_str, '.');
	if (splt_str == NULL)
		dt->precision = 0;
	else
	{
		dt->precision = ft_atoi(++splt_str);
		dt->precision = (dt->precision == 0) ? -1 : dt->precision;
	}
	splt_str = flag_adr;
	while (*flag_adr != '\0')
		++flag_adr;
	while ((flag_adr != splt_str) && !(ft_isdigit(*(flag_adr)) &&
		!ft_isdigit(*(flag_adr - 1)) && (*(flag_adr - 1) != '.')))
		--flag_adr;
	if ((*flag_adr == '0'))
		while ((flag_adr != splt_str) && !(ft_isdigit(*(flag_adr)) &&
			!ft_isdigit(*(flag_adr - 1)) && (*(flag_adr - 1) != '.')))
			--flag_adr;
	dt->min_size = ft_atoi(flag_adr);
	return (0);
}

int	ft_pars_and_print(char const *splt_str, t_data *dt)
{
	dt->size_flag = NULL;
	dt->format_flag = 0;
	dt->precision = 0;
	ft_get_var_id(splt_str, dt);
	if (ft_strrchr(splt_str, '*') != NULL)
		return (-1);
	if (splt_str[0] == '\0')
		return (0);
	else if (splt_str[0] == '%')
	{
		if (ft_get_var_id(splt_str, dt) == 0)
			return (-1);
		if (dt->size_flag == NULL)
			ft_get_size_flag(splt_str, splt_str, dt);
		ft_get_frmt_flag(splt_str, splt_str, dt);
		ft_get_precision(splt_str, splt_str, dt);
		return (1);
	}
	return (2);
}
