/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 15:14:37 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:55:47 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_pars_and_select(const char *str, va_list *var, int *nb_printed)
{
	int		selector;
	int		nb_added;
	t_data	dt;

	nb_added = 0;
	selector = ft_pars_and_print(str, &dt);
	if (selector == 2)
		nb_added = ft_print_reg_str(str);
	else if (selector == 1)
	{
		nb_added = ft_select_print_fct(dt, var, str);
	}
	if (*nb_printed >= 0)
		*nb_printed += nb_added;
	if (nb_added < 0)
		*nb_printed = -1;
	return (selector);
}

int			ft_printf(const char *restrict format, ...)
{
	va_list	lst_arg;
	char	**splt_str;
	int		i;
	int		nb_printed;
	int		nb_elem;

	if ((splt_str = ft_smart_split(format, &nb_elem)) == NULL)
		return (-1);
	i = -1;
	nb_printed = 0;
	va_start(lst_arg, format);
	while (splt_str[++i] != NULL)
		if (ft_pars_and_select(splt_str[i], &lst_arg, &nb_printed) == -1 ||
			nb_printed < 0)
			break ;
	ft_free_all(splt_str, &nb_elem);
	return (nb_printed);
}
