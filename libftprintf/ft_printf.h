/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 18:09:25 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:56:23 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdlib.h>
# include <stdarg.h>
# include "libft/libft.h"

typedef struct	s_data
{
	int			min_size;
	int			precision;
	char		format_flag;
	char		*size_flag;
	char		var_id;
}				t_data;

typedef enum	e_flags
{
	sharp_flag = 1,
	zero_flag = 2,
	minus_flag = 4,
	plus_flag = 8,
	space_flag = 16
}				t_flags;

int				ft_printf(const char *restrict format, ...);
char			**ft_smart_split(const char *str, int *nb_elem);
int				ft_pars_and_print(const char *splt_str, t_data *dt);
int				ft_print_c(va_list *prm, t_data dt);
int				ft_print_sc(const void *prm, t_data dt);
int				ft_print_s(va_list *prm, t_data dt);
int				ft_print_prvl_frmt(uintmax_t val, char is_sgnd, t_data dt);
int				ft_print_padding(uintmax_t val, char is_sgnd, t_data dt);
int				ft_print_d(va_list *number, t_data dt);
int				ft_print_reg_str(const char *str);
int				ft_select_print_fct(t_data dt, void *var, const char *str);
void			ft_free_all(char **str, int *nb_elem);
int				ft_print_percent(t_data dt, const char *str);
uintmax_t		ft_num_var_formater(uintmax_t val, t_data dt);
int				ft_num_length(uintmax_t val, char is_sgnd, t_data dt);
int				ft_prnt_dcml(uintmax_t uval, intmax_t val, t_data dt, char sgn);
#endif
