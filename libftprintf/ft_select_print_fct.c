/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select_print_fct.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 14:07:08 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:35:46 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_select_print_fct(t_data dt, void *var, const char *str)
{
	if (dt.var_id == 'x' || dt.var_id == 'X' || dt.var_id == 'o' ||
			dt.var_id == 'O' || dt.var_id == 'U' || dt.var_id == 'u' ||
			dt.var_id == 'd' || dt.var_id == 'D' || dt.var_id == 'i' ||
			dt.var_id == 'i')
		return (ft_print_d(var, dt));
	if (dt.var_id == 'c' || dt.var_id == 'C')
		return (ft_print_c(var, dt));
	if (dt.var_id == 's' || dt.var_id == 'S')
		return (ft_print_s(var, dt));
	if (dt.var_id == 'p')
	{
		dt.format_flag &= (minus_flag + zero_flag);
		dt.format_flag += sharp_flag;
		dt.size_flag = "l";
		return (ft_print_d(var, dt));
	}
	if (dt.var_id == '%')
		return (ft_print_percent(dt, str));
	return (0);
}
