/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_d.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 19:41:07 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:34:10 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static uintmax_t	ft_sz_poux(t_data dt, char *is_sgnd, va_list *val)
{
	uintmax_t usigned_val;

	if (ft_strstr(dt.size_flag, "ll"))
		usigned_val = va_arg(*val, unsigned long long);
	else if (ft_strstr(dt.size_flag, "l"))
		usigned_val = va_arg(*val, unsigned long);
	else if (ft_strstr(dt.size_flag, "hh"))
		usigned_val = (unsigned char)va_arg(*val, unsigned int);
	else if (ft_strstr(dt.size_flag, "h"))
		usigned_val = (unsigned short)va_arg(*val, unsigned int);
	else if (ft_strstr(dt.size_flag, "z"))
		usigned_val = va_arg(*val, size_t);
	else if (ft_strstr(dt.size_flag, "j"))
		usigned_val = va_arg(*val, uintmax_t);
	else
		usigned_val = va_arg(*val, int);
	*is_sgnd = 0;
	return (usigned_val);
}

static intmax_t		ft_sz_di(t_data dt, char *is_sgnd, va_list *val)
{
	intmax_t signed_val;

	if (ft_strstr(dt.size_flag, "ll"))
		signed_val = va_arg(*val, long long);
	else if (ft_strstr(dt.size_flag, "l"))
		signed_val = va_arg(*val, long);
	else if (ft_strstr(dt.size_flag, "hh"))
		signed_val = (signed char)va_arg(*val, int);
	else if (ft_strstr(dt.size_flag, "h"))
		signed_val = (short)va_arg(*val, int);
	else if (ft_strstr(dt.size_flag, "z"))
		signed_val = va_arg(*val, size_t);
	else if (ft_strstr(dt.size_flag, "j"))
		signed_val = va_arg(*val, intmax_t);
	else
		signed_val = va_arg(*val, int);
	*is_sgnd = signed_val < 0 ? 3 : 1;
	return (signed_val);
}

static int			ft_getv(t_data dt, va_list *nb, uintmax_t *uv, intmax_t *sv)
{
	char	is_sgnd;

	if (dt.var_id == 'd' || dt.var_id == 'i' || dt.var_id == 'D')
		*sv = ft_sz_di(dt, &is_sgnd, nb);
	else
		*uv = ft_sz_poux(dt, &is_sgnd, nb);
	return (is_sgnd);
}

int					ft_print_d(va_list *number, t_data dt)
{
	char			is_sgnd;
	uintmax_t		uval;
	intmax_t		val;
	int				char_printed;

	is_sgnd = ft_getv(dt, number, &uval, &val);
	char_printed = ft_prnt_dcml(uval, val, dt, is_sgnd);
	return (char_printed);
}
