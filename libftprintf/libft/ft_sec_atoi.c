/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:43:16 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/22 16:04:15 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_overflow_check(long long value, int signe, char *overflow_flag)
{
	if (-value > 2147483648)
		*overflow_flag = 1;
	if ((-value > 2147483647) && (signe == -1))
		*overflow_flag = 1;
}

int			ft_sec_atoi(const char *str, char *overflow_flag)
{
	int				signe;
	long long		result;
	int				i;

	*overflow_flag = 0;
	result = 0;
	i = 0;
	signe = 0;
	while ((*(str + i) == ' ') || (*(str + i) == '\t') ||
			(*(str + i) == '\v') || (*(str + i) == '\n') ||
			(*(str + i) == '\r') || (*(str + i) == '\f'))
		i++;
	if (*(str + i) == '-')
		signe = 1;
	else
		signe = (-1);
	if ((*(str + i) == '+') || (*(str + i) == '-'))
		i++;
	while (((ft_isdigit(*(str + i))) == 1) && *overflow_flag == 0)
	{
		result = result * 10 - (*(str + i) - '0');
		ft_overflow_check(result, signe, overflow_flag);
		i++;
	}
	return (result * signe);
}
