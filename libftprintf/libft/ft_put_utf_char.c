/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_utf_char.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 14:55:45 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/02 15:01:57 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

static	void	ft_print(unsigned short c)
{
	write(1, (void *)&c, 1);
}

void			ft_put_utf_char(const unsigned int c)
{
	int					mask;

	if (c > 127)
	{
		if (c >= 32768)
		{
			ft_print((0xF0 | ((c >> 15) & 0x07)));
		}
		if (c >= 2048)
		{
			mask = (c < 32768) ? 0x0F : 0x3F;
			ft_print((((c < 32768) ? 0xE0 : 0x80) | ((c >> 12) & mask)));
		}
		mask = (c < 127) ? 0x1F : 0x3F;
		ft_print(((c < 2048) ? 0xC0 : 0x80) | ((c >> 6) & mask));
		ft_print((0x80 | (c & 0x3F)));
	}
	else
		ft_print(c);
}
