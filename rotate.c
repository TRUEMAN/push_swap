/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 09:27:17 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/26 16:54:33 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ra(t_stack *stack_a)
{
	int tempo;
	int	i;

	if (stack_a->size < 2)
		return ;
	tempo = stack_a->tab[0];
	i = -1;
	while (++i < stack_a->size)
		stack_a->tab[i] = stack_a->tab[i + 1];
	stack_a->tab[stack_a->size - 1] = tempo;
}

void	rb(t_stack *stack_b)
{
	int tempo;
	int	i;

	if (stack_b->size < 2)
		return ;
	tempo = stack_b->tab[0];
	i = -1;
	while (++i < stack_b->size)
		stack_b->tab[i] = stack_b->tab[i + 1];
	stack_b->tab[stack_b->size - 1] = tempo;
}

void	rr(t_stack *stack_a, t_stack *stack_b)
{
	ra(stack_a);
	rb(stack_b);
}
