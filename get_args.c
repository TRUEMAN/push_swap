/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_args.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 10:33:23 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 18:52:25 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	fb_mtpl_val(int index_a, int index_b, int val, t_sflags flags)
{
	if (flags.pars_error_flag == 1)
	{
		ft_printf("La valeur %d est presente deux fois ", val);
		ft_printf("voir les valeur num %d & %d\n", index_a, index_b);
	}
	return (0);
}

static int	check_stack(t_stack s_a, t_sflags flags)
{
	int	to_check;
	int	index;

	to_check = -1;
	while (++to_check < s_a.size)
	{
		index = -1;
		while (++index < s_a.size)
			if ((s_a.tab[index] == s_a.tab[to_check]) && (index != to_check))
				return (fb_mtpl_val(to_check, index, s_a.tab[index], flags));
	}
	return (1);
}

static int	fb_of(int index, t_sflags flags)
{
	if (flags.pars_error_flag == 1)
		ft_printf("Overflow de la valeur num %d\n", index + 1);
	return (0);
}

static int	scan_for_error_input(char *input, int index, t_sflags flags)
{
	if (*input == '+' || *input == '-')
		if (*(input + 1) == '\0')
		{
			if (flags.pars_error_flag == 1)
				ft_printf("Saisie illegale de la valeur num %d\n", index + 1);
			return (0);
		}
	while (*input != '\0')
	{
		if (ft_isdigit(*input) != 1 && *input != '-' && *input != '+')
		{
			if (flags.pars_error_flag == 1)
				ft_printf("Saisie illegale de la valeur num %d\n", index + 1);
			return (0);
		}
		++input;
	}
	return (1);
}

int			get_args(char **val, t_stack *s_a, t_sflags flags)
{
	int		i;
	int		j;
	char	sgn_conflict_flag;
	char	overflow_flag;

	i = -1;
	overflow_flag = 0;
	while (!overflow_flag && ((++i) < (s_a->size)))
	{
		j = -1;
		if (val[i][0] == '\0')
			return (0);
		sgn_conflict_flag = 0;
		while ((val[i][++j] != '\0'))
		{
			if (val[i][j] != '-' || val[i][j] != '+')
				sgn_conflict_flag = sgn_conflict_flag == 0 ? 1 : -1;
			if ((scan_for_error_input(val[i], i, flags) == 0) ||
				(!ft_isdigit(val[i][j]) && ((val[i][j] != '-' ||
				val[i][j] != '+') && sgn_conflict_flag == -1)))
				return (0);
		}
		s_a->tab[i] = ft_sec_atoi(val[i], &overflow_flag);
	}
	return (overflow_flag == 0 ? check_stack(*s_a, flags) : fb_of(i, flags));
}
