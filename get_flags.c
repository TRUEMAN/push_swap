/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_flags.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 14:06:50 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 16:16:08 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	is_flag(char c)
{
	if (c == P_ERROR || c == CPT_OP || c == OP_OP || c == SHOW_STACK ||
		c == SHOW_BEGIN_STACK || c == SHOW_END_STACK || c == ALL_FLAGS ||
		c == HELP_FLAG || c == CHK_SUCCES)
		return (1);
	return (0);
}

static void	print_help(void)
{
	ft_putstr("Usage:\n\n\tpush_swap -h\n\tpush_swap -abcehopsS [valeurs]");
	ft_putstr("\n\n\t\t-h\n\t\t\tAffiche le manuel de push_swap,");
	ft_putstr(" le trie des piles ne s'effectue pas.");
	ft_putstr("\n\n\t\t-a\n\t\t\tActive tous les flags sauf -h.");
	ft_putstr("\n\n\t\t-c\n\t\t\tAffiche le nombre total d'operations");
	ft_putstr(" et le nombre d'operations par cathegories.");
	ft_putstr("\n\n\t\t-o\n\t\t\tExecute le push_swap en mode pas a pas.");
	ft_putstr("\n\n\t\t-e\n\t\t\tAffiche la stack apres execution.");
	ft_putstr("\n\n\t\t-b\n\t\t\tAffiche la stack avant execution.");
	ft_putstr("\n\n\t\t-s\n\t\t\tAffiche la stack pendant execution.");
	ft_putstr(" Active automatiquement les flags -e & -b.\n");
	ft_putstr("\n\n\t\t-p\n\t\t\tEn cas erreur dans les paramettres donne ");
	ft_putstr("a l'executable, donne le detail de l'erreur.\n");
	ft_putstr("\n\n\t\t-S\n\t\t\tVerifie si la pile est dans le bonne ordre ");
	ft_putstr("au terme de la resolution en affichant. Succes !! ou Fail !!\n");
}

static int	find_flags(char **argv, t_sflags *flags)
{
	int	i;

	i = 0;
	while ((argv[1][++i] != '\0') && (is_flag(argv[1][i])))
	{
		if (argv[1][i] == P_ERROR || argv[1][i] == ALL_FLAGS)
			flags->pars_error_flag = 1;
		if (argv[1][i] == CPT_OP || argv[1][i] == ALL_FLAGS)
			flags->cpt_op_flag = 1;
		if (argv[1][i] == OP_OP || argv[1][i] == ALL_FLAGS)
			flags->op_by_op_flag = 1;
		if (argv[1][i] == SHOW_STACK || argv[1][i] == ALL_FLAGS)
			flags->show_stack_flag = 1;
		if (argv[1][i] == SHOW_END_STACK || argv[1][i] == ALL_FLAGS ||
			argv[1][i] == SHOW_STACK)
			flags->show_end_stack_flag = 1;
		if (argv[1][i] == SHOW_BEGIN_STACK || argv[1][i] == ALL_FLAGS ||
			argv[1][i] == SHOW_STACK)
			flags->show_begin_stack = 1;
		if (argv[1][i] == CHK_SUCCES || argv[1][i] == ALL_FLAGS)
			flags->check_succes_flag = 1;
		if (is_flag(argv[1][i] == 0))
			return (-1);
	}
	return (i != 0 ? 1 : 0);
}

static void	setup_flags(t_sflags *flags)
{
	flags->pars_error_flag = 0;
	flags->cpt_op_flag = 0;
	flags->op_by_op_flag = 0;
	flags->show_stack_flag = 0;
	flags->show_end_stack_flag = 0;
	flags->show_begin_stack = 0;
	flags->check_succes_flag = 0;
}

int			get_flags(char **argv, t_sflags *flags)
{
	int	i;

	setup_flags(flags);
	if (argv[1][0] != '-' || ft_isdigit(argv[1][1]))
		return (0);
	i = 0;
	while (argv[1][++i] != '\0')
		if ((is_flag(argv[1][i]) == 0))
			return (-1);
	if (argv[1][1] == '\0')
		return (-1);
	if (ft_strchr(argv[1], HELP_FLAG))
	{
		print_help();
		return (-2);
	}
	return (find_flags(argv, flags));
}
