/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 08:51:58 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/04 17:56:08 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long long int	ft_power(int nb, int pow, char *check)
{
	long long int	value;
	long long int	last_value;

	value = 1;
	while (pow-- > 0)
	{
		last_value = value;
		value *= nb;
		if ((nb == 0) || (last_value != (value / nb)))
		{
			if (check != NULL)
				*check = 0;
			return (0);
		}
	}
	if (check != NULL)
		*check = 1;
	return (value);
}
