/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   r_rotate_cpt_print.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 11:14:56 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 18:53:06 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	r_rotate_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rra(s_a);
	add_to_answer(c_op->answer, "rra");
	c_op->rra++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nrra\n" : "rra ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	r_rotate_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rrb(s_b);
	add_to_answer(c_op->answer, "rrb");
	c_op->rrb++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nrrb\n" : "rrb ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	r_rotate_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rrr(s_a, s_b);
	add_to_answer(c_op->answer, "rrr");
	c_op->rrr++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nrrr\n" : "rrr ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}
