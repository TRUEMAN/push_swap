/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operation_optimizer.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 10:07:55 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 16:51:09 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	opt_swap_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	if ((s_a->size >= 2) && (s_a->tab[0] > s_a->tab[1]) && (s_b->size >= 2) &&
		(s_b->tab[0] < s_b->tab[1]))
	{
		swap_ab_cpt_print(s_a, s_b, c_op);
		return ;
	}
	if ((s_a->size >= 2) && (s_a->tab[0] > s_a->tab[1]))
	{
		swap_a_cpt_print(s_a, s_b, c_op);
		return ;
	}
	if ((s_b->size >= 2) && (s_b->tab[0] < s_b->tab[1]))
	{
		swap_b_cpt_print(s_a, s_b, c_op);
		return ;
	}
}

void	opt_swap_b(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	if ((s_a->size >= 2) && (s_a->tab[0] > s_a->tab[1]) && (s_b->size >= 2) &&
		(s_b->tab[0] < s_b->tab[1]))
	{
		swap_ab_cpt_print(s_a, s_b, c_op);
		return ;
	}
	if ((s_a->size >= 2) && (s_a->tab[0] > s_a->tab[1]))
	{
		swap_a_cpt_print(s_a, s_a, c_op);
		return ;
	}
	if ((s_b->size >= 2) && (s_b->tab[0] < s_b->tab[1]))
	{
		swap_b_cpt_print(s_a, s_b, c_op);
		return ;
	}
}

void	rot_clst_a(t_stack *sa, t_stack *sb, int val, t_op_cpt *cp)
{
	int	i;

	i = 0;
	if (sa->tab[0] < val)
		return ;
	while (i < sa->size / 2)
	{
		if (sa->tab[i + 1] < val)
		{
			rotate_a_cpt_print(sa, sb, cp);
			break ;
		}
		if (sa->tab[sa->size - i - 1] < val)
		{
			r_rotate_a_cpt_print(sa, sb, cp);
			break ;
		}
		++i;
	}
}

void	rot_clst_b(t_stack *s_a, t_stack *s_b, int val, t_op_cpt *c_op)
{
	int	i;

	i = 0;
	if (s_b->tab[0] >= val)
		return ;
	while (i < s_b->size / 2)
	{
		if (s_b->tab[i + 1] >= val)
		{
			rotate_b_cpt_print(s_a, s_b, c_op);
			break ;
		}
		if (s_b->tab[s_b->size - i - 1] >= val)
		{
			r_rotate_b_cpt_print(s_a, s_b, c_op);
			break ;
		}
		++i;
	}
}

void	rot_to_a(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op, int val)
{
	int	i;

	i = 0;
	if (s_a->tab[0] == val)
		return ;
	while (i <= s_a->size / 2 + 1)
	{
		if (s_a->tab[i + 1] == val)
		{
			rotate_a_cpt_print(s_a, s_b, c_op);
			break ;
		}
		if (s_a->tab[s_a->size - i - 1] == val)
		{
			r_rotate_a_cpt_print(s_a, s_b, c_op);
			break ;
		}
		++i;
	}
}
