# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vnoon <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/28 16:04:25 by vnoon             #+#    #+#              #
#    Updated: 2016/05/01 16:54:17 by vnoon            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=push_swap
SRC=counter_init.c flag_functions.c get_args.c get_flags.c get_split_point.c \
operation_optimizer.c push.c push_cpt_print.c push_swap.c r_rotate_cpt_print.c \
rev_rotate.c rotate.c rotate_cpt_print.c rotate_until_sort_a.c \
rotate_until_sort_b.c set_up_stacks.c solve.c stack_analitic.c swap.c \
swap_cpt_print.c wait.c big_solve.c
OBJ=$(SRC:.c=.o)
LIB=libft/
LIB_NAME=libft.a
LIB_PRINT=libftprintf/
LIB_PRINT_NAME=libftprintf.a
FLAG=-Wall -Wextra -Werror

$(NAME): $(OBJ)
	gcc -o $(NAME) $(OBJ) $(LIB)$(LIB_NAME) $(LIB_PRINT)$(LIB_PRINT_NAME)

all:$(NAME)

clean:
	make -C $(LIB) clean
	make -C $(LIB_PRINT) clean
	rm -f $(OBJ)

fclean:clean
	make -C $(LIB) fclean
	make -C $(LIB_PRINT) fclean
	rm -f $(NAME)

re:fclean all

%.o:%.c
	make -C $(LIB)
	make -C $(LIB_PRINT)
	gcc -c $(FLAG) $(SRC)
