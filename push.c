/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 09:40:49 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 15:41:23 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	stack_move_down(t_stack *stack)
{
	int	stack_size;

	if ((stack->size += 1) < 2)
		return ;
	stack_size = stack->size;
	while (--stack_size > 0)
		stack->tab[stack_size] = stack->tab[stack_size - 1];
}

static void	stack_move_up(t_stack *stack)
{
	int i;

	stack->size--;
	if (stack->size < 1)
		return ;
	i = -1;
	while (++i < stack->size)
		stack->tab[i] = stack->tab[i + 1];
}

void		pa(t_stack *stack_a, t_stack *stack_b)
{
	if (stack_b->size > 0)
	{
		stack_move_down(stack_a);
		stack_a->tab[0] = stack_b->tab[0];
		stack_move_up(stack_b);
	}
}

void		pb(t_stack *stack_a, t_stack *stack_b)
{
	if (stack_a->size > 0)
	{
		stack_move_down(stack_b);
		stack_b->tab[0] = stack_a->tab[0];
		stack_move_up(stack_a);
	}
}
