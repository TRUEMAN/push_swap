/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_cpt_print.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 11:04:01 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 18:53:45 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	rotate_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	ra(s_a);
	add_to_answer(c_op->answer, "ra");
	c_op->ra++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nra\n" : "ra ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	rotate_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rb(s_b);
	add_to_answer(c_op->answer, "rb");
	c_op->rb++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nrb\n" : "rb ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	rotate_ab_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	rr(s_a, s_a);
	add_to_answer(c_op->answer, "rr");
	c_op->rr++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\nrr\n" : "rr ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}
