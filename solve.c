/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/05 17:19:52 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 17:14:44 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	unload_stack(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	int	i;

	if (s_b->size == 0)
		return ;
	i = is_sort_until(*s_b, 1);
	while (i >= 0 && s_b->size != 0)
	{
		push_a_cpt_print(s_a, s_b, c_op);
		--i;
	}
}

void		sort_b(t_stack *s_a, t_stack *s_b, t_sflags flags, t_op_cpt *c_op)
{
	int	split_point;

	split_point = get_split_point_b(*s_b);
	while (is_there_above(*s_b, split_point) && s_b->size > 1)
	{
		if (s_b->tab[0] >= split_point)
		{
			opt_swap_b(s_a, s_b, c_op);
			push_a_cpt_print(s_a, s_b, c_op);
		}
		else
			rot_clst_b(s_a, s_b, split_point, c_op);
	}
	rotate_until_sort_b(s_a, s_b, c_op);
	while (s_b->size != 0 && is_sort_until(*s_b, 1) != -1)
		push_a_cpt_print(s_a, s_b, c_op);
	if (is_sort_until(*s_b, 1) != s_b->size - 1)
		return (sort_b(s_a, s_b, flags, c_op));
	else if (is_sort_until(*s_a, 0) != 0)
		return (sort_a(s_a, s_b, flags, c_op));
}

void		sort_a(t_stack *s_a, t_stack *s_b, t_sflags flags, t_op_cpt *c_op)
{
	int	split_point;

	split_point = get_split_point_a(*s_a);
	while (is_there_below(*s_a, split_point))
	{
		if (s_a->tab[0] < split_point)
		{
			opt_swap_a(s_a, s_b, c_op);
			push_b_cpt_print(s_a, s_b, c_op);
		}
		else
			rot_clst_a(s_a, s_b, split_point, c_op);
	}
	opt_swap_a(s_a, s_b, c_op);
	rotate_until_sort_a(s_a, s_b, c_op);
	if (is_sort_until(*s_a, 0) > 0)
		return (sort_a(s_a, s_b, flags, c_op));
	else if ((s_b->size >= 2) && (is_sort_until(*s_b, 1) != s_b->size - 1))
		return (sort_b(s_a, s_b, flags, c_op));
	if (is_sort(s_a->tab, s_a->size) == 0)
		return (sort_a(s_a, s_b, flags, c_op));
}

void		solve(t_stack *s_a, t_stack *s_b, t_sflags flags)
{
	t_op_cpt	c_op;

	counter_init(&c_op);
	c_op.flag = &flags;
	rotate_until_sort_a(s_a, s_b, &c_op);
	if (s_a->size > 2100 && is_sort(s_a->tab, s_a->size) == 0)
		big_solve(s_a, s_b, &c_op);
	while (is_sort(s_a->tab, s_a->size) == 0)
	{
		sort_a(s_a, s_b, flags, &c_op);
		unload_stack(s_a, s_b, &c_op);
	}
	if (flags.show_stack_flag == 0 && flags.op_by_op_flag == 0)
		print_solution(c_op.answer);
	if (is_sort(s_a->tab, s_a->size) == 1 && flags.check_succes_flag == 1)
		ft_printf("\nSucces !!\n");
	else if (flags.check_succes_flag == 1)
		ft_printf("\nFail !!\n");
	if (flags.cpt_op_flag == 1)
		show_op(c_op);
}
