/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   big_solve.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/01 16:12:44 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 17:12:17 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	*sorted_tab(t_stack *s_a)
{
	int	i;
	int	tempo;
	int	*mem_tab;

	mem_tab = (int *)malloc(sizeof(int) * s_a->size);
	if (mem_tab == NULL)
		return (NULL);
	i = -1;
	while (++i < s_a->size)
		mem_tab[i] = s_a->tab[i];
	while (is_sort(mem_tab, s_a->size) == 0)
	{
		i = -1;
		while (++i < s_a->size - 1)
			if (mem_tab[i] > mem_tab[i + 1])
			{
				tempo = mem_tab[i];
				mem_tab[i] = mem_tab[i + 1];
				mem_tab[i + 1] = tempo;
			}
	}
	return (mem_tab);
}

void		big_solve(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	int	*solve;
	int	index;

	solve = sorted_tab(s_a);
	index = s_a->size;
	while (s_a->size != 1)
	{
		while (solve[index - s_a->size] != s_a->tab[0])
			rot_to_a(s_a, s_b, c_op, solve[index - s_a->size]);
		push_b_cpt_print(s_a, s_b, c_op);
	}
	while (s_b->size != 0)
		push_a_cpt_print(s_a, s_b, c_op);
	free(solve);
}
