/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 09:14:10 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 16:51:25 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	send_error(void)
{
	ft_putstr_fd("Error\n", 2);
	return (1);
}

int			main(int argc, char **argv)
{
	t_stack		s_a;
	t_stack		s_b;
	t_sflags	flags;
	int			is_flg;

	if (argc < 2)
		return (0);
	if ((is_flg = get_flags(argv, &flags)) == -1)
		return (send_error());
	if (is_flg == -2)
		return (0);
	set_up_stacks(&s_a, &s_b, argc - 1 - is_flg);
	if (s_a.size <= 0)
		return (send_error());
	if (get_args((argv + 1 + is_flg), &s_a, flags) == 0)
		return (send_error());
	if (flags.show_begin_stack == 1)
		show_stack(s_a);
	solve(&s_a, &s_b, flags);
	if (flags.show_end_stack_flag == 1)
		show_stack(s_a);
	return (0);
}
