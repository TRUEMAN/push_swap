/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_rotate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/21 09:37:03 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/26 16:46:58 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	rra(t_stack *stack_a)
{
	int tempo;
	int	i;

	if (stack_a->size < 2)
		return ;
	tempo = stack_a->tab[(stack_a->size - 1)];
	i = stack_a->size;
	while (--i > 0)
		stack_a->tab[i] = stack_a->tab[i - 1];
	stack_a->tab[0] = tempo;
}

void	rrb(t_stack *stack_b)
{
	int tempo;
	int	i;

	if (stack_b->size < 2)
		return ;
	tempo = stack_b->tab[(stack_b->size - 1)];
	i = stack_b->size;
	while (--i > 0)
		stack_b->tab[i] = stack_b->tab[i - 1];
	stack_b->tab[0] = tempo;
}

void	rrr(t_stack *stack_a, t_stack *stack_b)
{
	rra(stack_a);
	rrb(stack_b);
}
