/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_split_point.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/22 15:52:54 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/01 17:09:09 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	fill_tab(int *tab, t_stack stack)
{
	int	i;

	i = -1;
	while (++i < stack.size)
		tab[i] = stack.tab[i];
}

static int	calculate_split_val_a(int *tab, int tab_len, t_stack stack)
{
	int	split_point;

	tab_len = is_sort_until(stack, 0) + 2;
	split_point = tab[tab_len % 2 == 0 ? (tab_len) / 2 : tab_len / 2];
	if (tab_len % 2 == 0 && tab_len > 3)
		split_point = split_point / 2 + (tab[(tab_len - 1) / 2] / 2);
	return (split_point);
}

int			get_split_point_a(t_stack stack)
{
	int	i;
	int	tempo;
	int	*mem_tab;
	int	split_point;

	if (stack.size == 1)
		return (stack.tab[0]);
	if ((mem_tab = (int *)malloc(sizeof(int) * stack.size)) == NULL)
		return (stack.tab[(stack.size - 1) / 2]);
	fill_tab(mem_tab, stack);
	while (is_sort(mem_tab, stack.size) == 0)
	{
		i = -1;
		while (++i < stack.size - 1)
			if (mem_tab[i] > mem_tab[i + 1])
			{
				tempo = mem_tab[i];
				mem_tab[i] = mem_tab[i + 1];
				mem_tab[i + 1] = tempo;
			}
	}
	split_point = calculate_split_val_a(mem_tab, stack.size, stack);
	free(mem_tab);
	return (split_point);
}

static int	calculate_split_val_b(int *tab, int tab_len, t_stack stack)
{
	int	split_point;
	int	i;
	int	j;

	j = 0;
	while (tab[0] != stack.tab[j])
		++j;
	i = 0;
	while (tab[i] == stack.tab[i <= j ? j - i : stack.size - i + j])
	{
		--tab_len;
		++i;
	}
	split_point = tab[(tab_len) / 2];
	if (tab_len % 2 == 0 && tab_len > 3)
		split_point = split_point / 2 + (tab[(tab_len - 1) / 2] / 2);
	return (split_point);
}

int			get_split_point_b(t_stack stack)
{
	int	i;
	int	tempo;
	int	*mem_tab;
	int	split_point;

	if (stack.size == 1)
		return (stack.tab[0]);
	if ((mem_tab = (int *)malloc(sizeof(int) * stack.size)) == NULL)
		return (stack.tab[(stack.size - 1) / 2]);
	fill_tab(mem_tab, stack);
	while (is_sort(mem_tab, stack.size) == 0)
	{
		i = -1;
		while (++i < stack.size - 1)
			if (mem_tab[i] > mem_tab[i + 1])
			{
				tempo = mem_tab[i];
				mem_tab[i] = mem_tab[i + 1];
				mem_tab[i + 1] = tempo;
			}
	}
	split_point = calculate_split_val_b(mem_tab, stack.size, stack);
	free(mem_tab);
	return (split_point);
}
