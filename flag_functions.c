/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/26 17:45:14 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/22 15:40:22 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	show_stack(t_stack stack)
{
	int	i;

	i = -1;
	ft_printf("\nStack index \tStack value\n");
	while (++i < stack.size)
		ft_printf("%12d\t%12d\n", i, stack.tab[i]);
}

void	show_stack_ab(t_stack stack_a, t_stack stack_b)
{
	int	i;

	i = -1;
	ft_putchar('\n');
	ft_printf("Stack a index\tStack a value\t|Stack b index\tStack b value\n");
	while ((++i < stack_a.size) || (i < stack_b.size))
	{
		if (i < stack_a.size)
			ft_printf("%12d\t%12d\t|", i, stack_a.tab[i]);
		else
			ft_printf("%12d\t%12s\t|", i, "Vide");
		if (i < stack_b.size)
			ft_printf("%12d\t%12d\n", i, stack_b.tab[i]);
		else
			ft_printf("%12d\t%12s\n", i, "Vide");
	}
}

void	show_op(t_op_cpt c_op)
{
	int	total;

	ft_printf("\n------Detail des operations------\n\n");
	ft_printf("pa: %d\n", c_op.pa);
	ft_printf("pb: %d\n", c_op.pb);
	ft_printf("sa: %d\n", c_op.sa);
	ft_printf("sb: %d\n", c_op.sb);
	ft_printf("ss: %d\n", c_op.ss);
	ft_printf("ra: %d\n", c_op.ra);
	ft_printf("rb: %d\n", c_op.rb);
	ft_printf("rr: %d\n", c_op.rr);
	ft_printf("rra: %d\n", c_op.rra);
	ft_printf("rrb: %d\n", c_op.rrb);
	ft_printf("rrr: %d\n", c_op.rrr);
	total = 0;
	total = c_op.pa + c_op.pb + c_op.sa + c_op.sb + c_op.ss + c_op.ra + c_op.rb;
	total += c_op.rr + c_op.rra + c_op.rrb + c_op.rrr;
	ft_printf("\nTotal des operations: %d\n", total);
}
