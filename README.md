Mon push_swap dispose de divers flags, ils sont mentionnés dans l'aide. Pour les voir dans le detail.
Mon push_swap n'accepte les flags que si ils sont spécifiés avant les valeurs a trier. 

push_swap git:(master) ✗ ./push_swap -h 1 0
Usage:

	push_swap -h
	push_swap -abcehopsS [valeurs]

		-h
			Affiche le manuel de push_swap, le trie des piles ne s'effectue pas.

		-a
			Active tous les flags sauf -h.

		-c
			Affiche le nombre total d'operations et le nombre d'operations par cathegories.

		-o
			Execute le push_swap en mode pas a pas.

		-e
			Affiche la stack apres execution.

		-b
			Affiche la stack avant execution.

		-s
			Affiche la stack pendant execution. Active automatiquement les flags -e & -b.


		-p
			En cas erreur dans les paramettres donne a l'executable, donne le detail de l'erreur.


		-S
			Verifie si la pile est dans le bonne ordre au terme de la resolution en affichant. Succes !! ou Fail !!