/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_cpt_print.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/08 10:31:20 by vnoon             #+#    #+#             */
/*   Updated: 2016/04/30 18:52:39 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_a_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	pa(s_a, s_b);
	add_to_answer(c_op->answer, "pa");
	c_op->pa++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\npa\n" : "pa ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}

void	push_b_cpt_print(t_stack *s_a, t_stack *s_b, t_op_cpt *c_op)
{
	pb(s_a, s_b);
	add_to_answer(c_op->answer, "pb");
	c_op->pb++;
	if (c_op->flag->show_stack_flag == 1 || c_op->flag->op_by_op_flag == 1)
	{
		ft_printf("%s", c_op->flag->show_stack_flag == 1 ? "\npb\n" : "pb ");
		if (c_op->flag->show_stack_flag == 1)
			show_stack_ab(*s_a, *s_b);
		if (c_op->flag->op_by_op_flag == 1)
			wait_for_enter();
	}
}
